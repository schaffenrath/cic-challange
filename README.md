# SF CO2 Emission Service (CIC-Challenge)

## Description

---

The **SF CO2 Emission Service** is a simple web service that provides the user with information about the annual CO2 emissions and type of energy source for public buildings in San Francisco. It dynamically requests this data from the open open database **DataSF** and extracts the properties: _'department', 'source_type'_ and _'emissions_mtco2e'_.

## Usage

---

Building the project will produce a web archive file (_.war_), which can simply be deployed by a web server. Accessing the service can be done by connecting to the web server, followed by the path:

"_/CICChallenge/webapi/department_".

Additionally a query parameter (_departmentStartsWith_) can be used to filter departments. This query parameter is separated from the URL by the _?_ symbol. For instance, to get all departments that start with the letters "Mun" one can simply make the request:

"_/CICChallenge/webapi/department?departmentStartsWith=Mun_".

A simple GUI is available that displays the requested data into a table and allows filtering via an input field. This GUI can be accessed by:

"_/CICChallenge/_".

## Running Instance

---

An instance of this service is hosted by a Raspberry Pi 4 under the link [rsbpi.ddns.net:8080](http://rsbpi.ddns.net:8080/CICChallenge/).
