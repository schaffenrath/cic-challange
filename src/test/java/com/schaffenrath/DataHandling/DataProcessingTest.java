package com.schaffenrath.DataHandling;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class DataProcessingTest {

	private final DataProcessing dataProcessing = new DataProcessing();

	@DisplayName("Test filter and convertion to JSON from invalid data")
	@Test
	public void testInvalidFilterAndJSONConvertion() {
		assertTrue(this.dataProcessing.filterAndConvertResponseToJSON(null).isEmpty());
		assertTrue(this.dataProcessing.filterAndConvertResponseToJSON("").isEmpty());
		assertTrue(this.dataProcessing.filterAndConvertResponseToJSON("[]").isEmpty());
		assertThrows(JSONException.class, () -> this.dataProcessing.filterAndConvertResponseToJSON("{}"));
		assertThrows(JSONException.class,
				() -> this.dataProcessing.filterAndConvertResponseToJSON("{\"department\": \"Parks and Recreation\"}"));
	}

	@DisplayName("Test filter and convertion to JSON from valid data")
	@Test
	public void testValidFilterAndJsonconverstion() {
		// Create test data
		JSONObject testObject = new JSONObject();
		testObject.put("department", "TestPlaceA");
		testObject.put("source_type", "Gasoline");
		testObject.put("emissions_mtco2e", "3.753");
		testObject.put("fiscal_year", "2015");
		JSONArray testArray = new JSONArray();
		testArray.put(testObject);

		JSONArray processedArray = this.dataProcessing.filterAndConvertResponseToJSON(testArray.toString());
		assertEquals(1, processedArray.length());
		JSONObject processedObject = processedArray.getJSONObject(0);
		assertTrue(((String) processedObject.get("department")).equals("TestPlaceA"));
		assertTrue(((String) processedObject.get("source_type")).equals("Gasoline"));
		assertTrue(((String) processedObject.get("emissions_mtco2e")).equals("3.753"));
		// Should throw exception, since fiscal_year is not a targeted property
		assertThrows(JSONException.class, () -> ((String) processedObject.get("fiscal_year")).equals("2015"));

	}
}
