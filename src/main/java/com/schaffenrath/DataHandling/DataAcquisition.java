package com.schaffenrath.DataHandling;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

public class DataAcquisition {
	private final String apiEndpoint = "https://data.sfgov.org/resource/pxac-sadh.json";
	private final String apiToken = "$$app_token=57QY4rbFhx3RCuiMQwh4i7FnU";
	// URL encoding for: 'emissions_mtco2e>0'
	private final String apiEmissionCondition = "emissions_mtco2e%3E0";

	private final String apiDepartmentFilter(String startsWith) {
		return "starts_with(department,%22" + startsWith + "%22)";
	}

	// @VisibleForTesting
	protected final URL buildAPIRequestURL(final String departmentStartsWith) throws MalformedURLException {
		String apiRequestURL = apiEndpoint + "?$where=" + apiEmissionCondition;
		if (!(departmentStartsWith == null))
			apiRequestURL = apiRequestURL + "%20and%20" + apiDepartmentFilter(departmentStartsWith);
		apiRequestURL = apiRequestURL + "&" + apiToken;
		return new URL(apiRequestURL);
	}

	public String requestDepartmentCo2Data(String departmentStartsWith) {
		try {
			URL apiRequesURL = buildAPIRequestURL(departmentStartsWith);
			HttpsURLConnection apiConnection = (HttpsURLConnection) apiRequesURL.openConnection();
			apiConnection.setConnectTimeout(2000);
			apiConnection.setRequestProperty("accept", "application/json");
			InputStream apiResponseStream = apiConnection.getInputStream();
			return new String(apiResponseStream.readAllBytes(), StandardCharsets.UTF_8);
		} catch (MalformedURLException e) {
			System.err.println("Failed to create request URL: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("Failed to connect to API Endpoint: " + e.getMessage());
		}
		return "Failed to retrieve data!";
	}
}
