<html>
<head>
<title>SF CO2 Emissions</title>
<style>
	.heading {
		font-family: Verdana, Geneva, sans-serfi;
		font-size: 30px;
		text-align: center;
		margin-top: 20px;
		margin-bottom: 30px;
	}
</style>
<script src="script.js"></script>
</head>
<body>
	<h2 class="heading">SF CO2 Emissions of public buildings</h2>
	<div style="width: 100%; height: 0px; border: 1px solid black;"></div>
	<div style="flex">
		<input type="text" id="departmentFilter" name="depFilter" placeholder="Department filter"/>
		<button style="margin-top: 20px; margin-left: 10px; height: 25px; width: 120px;" onclick="requestCo2Data();">Request data</button>
	</div>
	<div id="tableData"></div>
</body>
</html>
